=======================
pytest-polarion-collect
=======================

pytest plugin for collecting test cases and corresponding Polarion data.


Usage
-----
Collect tests data::

    $ py.test --collect-only --generate-json

Install
-------
Install this plugin::

    $ pip install pytest-polarion-collect
